# Изменённый README
## Задание 1.1.1 - 1.1.13
### Первое изменение
Текст первого изменения
### Второе изменение
Текст второго изменения
## Задание 1.2.1 - 1.2.5
Конфигурация [.gitignore](.gitignore) позволяет игнорировать при коммитах папки `.idea` любого уровня вложенности.
Конфигурация [terraform/.gitignore](terraform/.gitignore) позволяет игнорировать при коммитах:
- папки `.terraform` любого уровня вложенности;
- а также файлы:
  - *.tfstate
  - *.tfstate.*
  - crash.log
  - *.tfvars
  - override.tf
  - override.tf.json
  - *_override.tf
  - *_override.tf.json
  - .terraformrc
  - terraform.rc

_Студент: Дмитрий Ю._